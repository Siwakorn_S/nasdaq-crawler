import path from "path";
import stockApi from './api/stock'
export default (app) => {
    app.use("/api/stocks", stockApi);
    
    app.route("/*").get((req, res) => {
        res.status(404).send('Not found api')
    });
}