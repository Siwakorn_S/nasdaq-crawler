import { cronJobTimer, getNasdaqIndex } from '../../src/jobs/crawler'
import parser from 'cron-parser';
import moment from 'moment';

describe('crawler www.nasdaq.com', () => {
    test('Parse data correctly', async () => {
        const mockResponse = {
            Value: 2733.44
        }
        fetch.mockResponseSuccess(JSON.stringify(mockResponse));

        const data = await getNasdaqIndex();
        expect(fetch).toHaveBeenCalled()
        expect(data).toEqual(mockResponse.Value)
        expect(data).toBeGreaterThanOrEqual(0)
        expect(data).toMatchSnapshot()
    })

    test('Parse data error', async () => {
        const mockError = new Error('Get error message');
        fetch.mockResponseFailure(mockError);
        try {
            const data = await getNasdaqIndex();
        } catch (error) {
            expect(fetch).toHaveBeenCalled()
            expect(error).toEqual(mockError)
            expect(error).toBeInstanceOf(Error)
            expect(error).toMatchSnapshot();
        }
    })
})

describe('Run cronJob', () => {
    test('Run it correctly', () => {
        const interval = parser.parseExpression(cronJobTimer);
        const start = moment(new Date(interval.next().toString()));
        const stop = moment(new Date(interval.next().toString()));
        const diffTime = stop.diff(start, 'seconds')
        expect(diffTime).toBeGreaterThan(0);
    })
})