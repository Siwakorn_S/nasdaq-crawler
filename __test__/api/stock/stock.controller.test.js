import moment from 'moment';

describe('Stock Api', () => {
    test('getStocks correctly', async () => {
        const mockResponse = {
            stocks: [
                {
                    stock: 'Nasdaq',
                    value: 1718.22,
                    createdOn: moment('30-11-2017', 'DD-MM-YYYY')
                },
                {
                    stock: 'Nasdaq',
                    value: 1718.83,
                    createdOn: moment('17-12-2017', 'DD-MM-YYYY')
                }
            ]
        }

        fetch.mockResponseSuccess(JSON.stringify(mockResponse));
        const response = await fetch('http://localhost:9000/stocks');
        const result = await response.json();
        expect(result.stocks).toHaveLength(2)
        expect(result).toMatchSnapshot();
    })

    test('getStocks error', async () => {
        const mockError = new Error('Get error message');
        fetch.mockResponseFailure(mockError);
        try {
            const result = await fetch('http://localhost:9000/stocks');
        } catch (error) {
            expect(error).toEqual(new Error('Get error message'))
            expect(error).toBeInstanceOf(Error)
            expect(error).toMatchSnapshot();
        }
    })
})