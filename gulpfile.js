const gulp = require('gulp');
const sourcemaps = require('gulp-sourcemaps');
const babel = require('gulp-babel');
const concat = require('gulp-concat');

const uglify = require('gulp-uglify');
const rimraf = require('gulp-rimraf')

gulp.task('clean:dist', () => {
    return gulp.src('dist', { read: false })
        .pipe(rimraf());
})


gulp.task('transplie', () =>
    gulp.src('src/**/*.js')
        .pipe(babel({
            presets: ['env']
        }))
        .pipe(uglify())
        .pipe(gulp.dest('dist'))
);

gulp.task('build', ['clean:dist', 'transplie']);



