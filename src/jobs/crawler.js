import schedule from 'node-schedule'
import Stock from './../api/stock/stock.model'
import fetch from 'isomorphic-unfetch';
import 'isomorphic-form-data';

const NASDAQ_VALUE_SELECTOR = 'indexTable';

export const cronJobTimer = '*/10 * * * * *'
let browser
export const getNasdaqIndex = async () => {
    try {
        const formData = new FormData();
        formData.append("index", "ixic");
        const response = await fetch("http://www.nasdaq.com/aspx/IndexData.ashx", { method: 'POST', body: formData });
        const data = await response.json();
        return data.Value
    } catch (error) {
        console.error('Error', 'getNasdaqIndex', error)
        return error;
    }
}

export const cancelSchedule = schedule.scheduleJob(cronJobTimer, async () => {
    try {
        const nasdaqIndex = await getNasdaqIndex()
        await Stock.create({ 'stock': 'nasdaq', value: parseFloat(nasdaqIndex) })
    } catch (error) {
        cancelSchedule()
    }
});

process.on('exit', () => {
    cancelSchedule();
})