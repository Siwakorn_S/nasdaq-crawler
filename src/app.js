import express from 'express';
import mongoose from 'mongoose';
import registerRoutes from './routes';
import 'isomorphic-form-data';
//Start Scheduler Jobs
import './jobs/'

const app = express();

// Connect to MongoDB
mongoose.connect('mongodb://admin:admin@ds119486.mlab.com:19486/nasdaq');
mongoose.connection.on('error', (err) => {
    console.error(`MongoDB connection error: ${err}`);
    process.exit(-1); // eslint-disable-line no-process-exit
}).on('open', () => {
    console.log('Connected to MongoDb')
});

registerRoutes(app)


app.listen(16666, () => {
    console.log('Server listen on port', 16666)
})


