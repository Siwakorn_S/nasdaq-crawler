import express from "express"
const controller = require("./stock.controller");

const router = express.Router();
router.get("/", controller.index);

export default router;

