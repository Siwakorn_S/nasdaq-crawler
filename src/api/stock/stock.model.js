"use strict";
import mongoose from "mongoose";

const StockSchema = new mongoose.Schema({
    stock: String,
    value: Number,
    createdOn: Date,
});

StockSchema.pre("save", function (next) {
    if (!this.createdOn) {
        this.createdOn = new Date();
    }
    next();
});
export default mongoose.model("Stock", StockSchema);
