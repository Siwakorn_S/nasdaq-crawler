import Stock from './stock.model'

export const index = async (req, res) => {
    const skip = parseInt(req.query.skip) || 0;
    const limit = parseInt(req.query.limit) || 100;
    const sortBy = req.query.sortBy || "-createdOn";
    try {
        const data = await Stock.find({}).sort(sortBy).skip(skip).limit(limit).lean().exec()
        res.status(200).send(data);
    } catch (error) {
        res.status(500).send(error);
    }
}

export default { index }